from flask import *

app = Flask(__name__)

@app.route("/")
def index():
    return "This is an Index!"
  

@app.route("/hello")
def hello():
    return "Hello World!"

@app.route("/hello/<string:name>/")
def helloPerson(name):
    return render_template('hello.html', name=name)

@app.route("/members")
def members():
    return "Members"

@app.route("/members/<string:name>/")
def getMember(name):
    return name
  

if __name__ == "__main__":
  app.run()
